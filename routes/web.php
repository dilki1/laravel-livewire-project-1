<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Livewire\Contact;
use App\Http\Livewire\User;
use App\Http\Livewire\Manager;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home',[HomeController::class, 'test'])->name('home.test');
Route::get('/userSystem', function(){
    return view('user');
});

//Route::view('contacts', 'users.contacts');

Route::get('/new',Contact::class);//Contact mean livewire component class name
Route::get('/User',User::class);
Route::get('/managerDetails',Manager::class);
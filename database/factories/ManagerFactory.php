<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ManagerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'Name' => $this->faker->Name,
            'Date' => $this->faker->Date,
            'From' => $this->faker->From,
            'To' => $this->faker->To,
            'Resean' => $this->faker->Resean
            
            //
        ];
    }
}

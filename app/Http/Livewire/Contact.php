<?php

namespace App\Http\Livewire;

use Livewire\Component;

//Livewire componenet file
class Contact extends Component

{
    public $total;

    public function totalOfNumbers($num1, $num2){

        $this->total = $num1 + $num2;
    }
    public function render()
    {
        return view('livewire.contact')
            ->layout('layouts.app');
    }
}

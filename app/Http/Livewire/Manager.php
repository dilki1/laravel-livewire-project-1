<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Manager as ManagerModal;

class Manager extends Component
{

    public $name;
    public $date;
    public $from;
    public $to;
    public $reason;
    public $data;

    public function render()
    {
        $this->data=ManagerModal::all();
        $manager = ManagerModal::orderBy('id','DESC')->get();
        return view('livewire.manager',['manager'=>$manager]);
    }

    public function saveManagers(){
        $this->name = '';
        $this->date = '';
        $this->from = '';
        $this->to = '';
        $this->reason = '';
    }

    public function store(){
         $this->validate([
            'name' => 'required',
            'date'=> 'required',
            'from'=> 'required',
            'to' => 'required',
            'reason' => 'required'
        ]);

        Manager::create([
            'name' => $this->name,
            'date'=>  $this->date,
            'from'=> $this->from,
            'to' => $this->to,
            'reason' => $this->reason
        ]);
        $this->saveManagers();

    }

        // Manager::create($validateData);
        // session()->flash('message','Successfully Saved Manager!');
        //$this->saveManagers();
        // $this->emit('addManagers');
    
   
}

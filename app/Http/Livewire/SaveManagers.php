<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SaveManagers extends Component
{
    public function render()
    {
        return view('livewire.save-managers');
    }
}

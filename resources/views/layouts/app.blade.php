<!DOCTYPE html>
    <html>
        <head>
        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
        </head>
        <body>
        {{ $slot }}
        <script>
            // window.on('addManagers',()=>{  
            //     $("#exampleModal").hide();
            // });
           
        </script>
            <!-- <div>
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="card">
                                <div class="card-header">
                                    <h3>LEAVE MANAGER</h3>
                                </div>
                                <div class="card-body">
                                    <form>
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-6 form-group">
                                                    
                                                        <label
                                                            ><span class="icon-hand-right"></span>&nbsp;Name</label
                                                        >
                                                        <input type="text" class="form-controller" require/>
                                                    
                                                    
                                                </div>
                                                <div class="col-lg-6 form-group">
                                                
                                                        <label
                                                            ><span class="icon-hand-right"></span>&nbsp;Date</label
                                                        >
                                                        <input type="text" class="form-controller" require/>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-6 form-group">
                                                
                                                        <label
                                                            ><span class="icon-hand-right"></span>&nbsp;From</label
                                                        >
                                                        <input type="text" class="form-controller" require/>
                                                   
                                                </div>
                                                <div class="col-lg-6 form-group">
                                                    
                                                        <label
                                                            ><span class="icon-hand-right"></span>&nbsp;To</label
                                                        >
                                                        <input type="text" class="form-controller" require/>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 form-group row justify-content-center">
                                            <div class="col-lg-4 form-group">
                                                <button
                                                type="button"
                                                id="btnSubmit"
                                                class="btn btn-primary"
                                                
                                                >
                                                Save
                                                </button>

                                            
                                            </div>
                                            
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div> -->
        </body>
       
    </html>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
</head>
<body>
    {{ $slot }}
    @stack('modals')
    @livewireScripts
</body>
</html>
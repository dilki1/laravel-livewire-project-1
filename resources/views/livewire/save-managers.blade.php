

 <!-- Modal -->
 <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center">Add Managers</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <label>Name</label>
                                                <input type="text" class="form-control" wire:model='name' required></input>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <label>Date</label>
                                                <input type="text" class="form-control" wire:model='date' required></input>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <label>From</label>
                                                    <input type="text" class="form-control" wire:model='from' required></input>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <label>To</label>
                                                    <input type="text" class="form-control" wire:model='to' required></input>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="col-lg-6">
                                            <div class="row">
                                                <label>Reason</label>
                                                <input type="text" class="form-control" wire:model='reason' required></input>
                                            </div>
                                        </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" wire:click="store()">Save</button>
                                </div>
                                </div>
                            </div>
                            </div>
